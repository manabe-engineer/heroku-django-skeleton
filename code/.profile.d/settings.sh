#!/usr/bin/env bash
set -ex

sed -ri -e "s!# import django_heroku!import django_heroku!g" /app/app/settings.py
sed -ri -e "s!# django_heroku.settings(locals())!django_heroku.settings(locals())!g" /app/app/settings.py
